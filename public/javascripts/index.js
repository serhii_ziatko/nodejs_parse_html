

document.getElementById('form').addEventListener('submit', function(e) {
  e.preventDefault();
  let val = document.getElementById('form_input').value
  const for_img = document.getElementById('for_img');
  const xhr  = new XMLHttpRequest();
  xhr.open('POST', 'http://localhost:3000', false);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

  xhr.onload = function(){
    var arr = JSON.parse(xhr.response);
    console.log(xhr.response);
    arr.map(function(item) {
        var img = document.createElement('img');
        img.setAttribute('src', item);
        for_img.appendChild(img);
    })
  };
  xhr.send('site=' + val );
  
}, false)
const express = require('express');
const router = express.Router();
const request = require('request');
const cheerio = require('cheerio');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('');
});

router.post('/', function(req, res, next){
  console.log(req.body.site)
  request('http://' + req.body.site, function(error, response, body){
    if (!error && response.statusCode == 200 ){
      
      const $ = cheerio.load(body);
      const arr = [];
      $('img').map(function(){
        arr.push('http://' + req.body.site +'/' + $(this).attr('src'))
      })
      console.log(arr)
      res.json(arr);
      
    }
  })
});

module.exports = router;
